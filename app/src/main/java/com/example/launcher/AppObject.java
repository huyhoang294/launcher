package com.example.launcher;

import android.graphics.drawable.Drawable;

public class AppObject {
    private String  name,
            packageName;
    private Drawable image;
    private long versionCode;
    private String versionName;

    public AppObject(String packageName, String name, Drawable image,long versionCode,String versionName ){
        this.name = name;
        this.packageName = packageName;
        this.image = image;
        this.versionName = versionName;
        this.versionCode = versionCode;

    }

    public String getPackageName(){return packageName;}
    public String getName(){return name;}
    public Drawable getImage(){return image;}
    public long getVersionCode(){return versionCode;}
    public String getVersionName(){return versionName;}

}
